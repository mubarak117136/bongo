a = {
    "key1": 1,
    "key2": {
        "key3": 1,
        "key4": {
            "key5": 4
        }
    }
}
i = 0
def print_depth(data):
    global i; i = i + 1
    if isinstance(data, dict):
        for key, item in data.items():
            print(f"{key} {i}")
            print_depth(item) if isinstance(item, dict) else ""
    else:
        raise Exception("Require dict to execute the function!")
print_depth(a)
