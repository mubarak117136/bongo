class Person(object):
    def __init__(self, first_name, last_name, father):
        self.first_name = first_name
        self.last_name = last_name
        self.father = father

person_a = Person("User", "1", None)
person_b = Person("User", "2", person_a)

a = {
    "key1": 1,
    "key2": {
        "key3": 1,
        "key4": {
            "key5": 4,
            "user": person_b
        }
    }
}

def handle_obj(obj):
    if isinstance(obj, dict):
        data = {}
        for k, v in obj.items():
            data[k] = handle_obj(v)
        return data
    elif isinstance(obj, Person):
        return handle_obj(obj.__dict__)
    else:
        return obj

i = 0
def print_depth(data):
    global i; i = i + 1
    data = handle_obj(data)
    if isinstance(data, dict):
        for key, item in data.items():
            print(f"{key} {i}")
            print_depth(item) if isinstance(item, dict) else ""
    else:
        raise Exception("Require dict to execute the function!")

print_depth(a)
